package Ortega.rest;

import Ortega.ConnectionPool.ConnectionPool;
import Ortega.Connectivity.OConnection;
import Ortega.rest.Connection.CP;
import com.google.gson.JsonObject;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.Produces;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;

@Path("register")
public class RegisterResource {
    private static ConnectionPool cp = new CP(1, 2).getConnectionPool();
    
    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public String register(
            @FormParam(value = "email") String email, 
            @FormParam(value = "password") String password, 
            @FormParam(value = "name") String name) 
    {
        if(email.isEmpty() || password.isEmpty() || name.isEmpty()){
            JsonObject jsonResponse = new JsonObject();
            jsonResponse.addProperty("result", false);
            jsonResponse.addProperty("msg", "One or more required fields are empty.");
            return jsonResponse.toString();
        }
        
        OConnection conn = cp.getConnection(3);
        if(conn == null){
            JsonObject jsonResponse = new JsonObject();
            jsonResponse.addProperty("result", false);
            jsonResponse.addProperty("msg", "Server is busy.");
            return jsonResponse.toString();
        }
        
        try {
            conn.statement("INSERT INTO users(email_use, pass_use, name_use) "
                    + "VALUES(?, ?, ?)", email, password, name);
            conn.commit();
            
            JsonObject jsonResponse = new JsonObject();
            jsonResponse.addProperty("result", true);
            jsonResponse.addProperty("msg", "User has been created successfully");
            return jsonResponse.toString();
        } catch(SQLException ex){
            JsonObject jsonResponse = new JsonObject();
            jsonResponse.addProperty("result", false);
            
            if(ex.getSQLState().equals("23505")) 
                jsonResponse.addProperty("msg", "That email address is already beign used.");
            else {
                System.err.println("An exception has ocurred while registering an "
                        + "user. Exception: " + ex);
                
                try {
                    System.err.println("Attempting to rollback after error: " + ex);
                    conn.rollback();
                    System.err.println("Rollback successful for error: " + ex);
                } catch (SQLException ex1) {
                    System.err.println("An error has occurred while rolling back "
                            + "after an error Inside the contacts resource. "
                        + "Error: " + ex1);
                    System.err.println("Attempting to close connection " + conn.getID());
                    try {
                        conn.close();
                    } catch (SQLException ex2) {
                        Logger.getLogger(ContactsResource.class.getName()).log(Level.SEVERE, null, ex2);
                    }
                }
                
                jsonResponse.addProperty("msg", "User could not be created "
                        + "due to internal server error.");
            }
            
            return jsonResponse.toString();
        } finally {
            cp.returnConnection(conn);
        }
    }
}