package Ortega.rest;

import Ortega.ConnectionPool.ConnectionPool;
import Ortega.Connectivity.OConnection;
import Ortega.rest.Connection.CP;
import com.google.gson.*;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.Date;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.Produces;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import org.postgresql.util.Base64;

@Path("Session")
public class SessionResource {
    private static final ConnectionPool cp = new CP(1, 2).getConnectionPool();
    
    @POST
    @Path("login")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public String login(
            @FormParam("email") String email, 
            @FormParam("password") String password) 
    {
        if(email.isEmpty() || password.isEmpty()){
            JsonObject jsonResponse = new JsonObject();
            jsonResponse.addProperty("result", false);
            jsonResponse.addProperty("msg", "One or more required fields are empty.");
            return jsonResponse.toString();
        }
        
        OConnection conn = cp.getConnection(4);
        if(conn == null){
            JsonObject jsonResponse = new JsonObject();
            jsonResponse.addProperty("result", false);
            jsonResponse.addProperty("msg", "Server is busy.");
            return jsonResponse.toString();
        }
        
        try{
            Object[][] loginResult = conn.query("SELECT id_use, name_use, email_use "
                    + "FROM users WHERE(email_use = ?) AND(pass_use = ?)", 
                    email, password);
            
            //If user is valid, generate token
            if(loginResult.length > 1){
                long timeStamp = new Date().getTime();
                Random rd = new Random();
                String token = "Rdm:" + rd.nextInt(100000) + "::Adr:" + email + "::Ltm:" + timeStamp;
                String encodedToken = Base64.encodeBytes(token.getBytes("ISO-8859-1"));
                
                conn.statement("UPDATE users SET authtoken_use = ? WHERE id_use = ?", 
                        encodedToken, loginResult[1][0]);
                conn.commit();
                
                JsonObject jsonResponse = new JsonObject();
                jsonResponse.addProperty("result", true);
                jsonResponse.addProperty("name", (String) loginResult[1][1]);
                jsonResponse.addProperty("email", (String) loginResult[1][2]);
                jsonResponse.addProperty("token", encodedToken);
                return jsonResponse.toString();
            } else {
                JsonObject jsonResponse = new JsonObject();
                jsonResponse.addProperty("result", false);
                jsonResponse.addProperty("msg", "User not found or invalid "
                        + "password. Please, check your credentials.");
                return jsonResponse.toString();
            }
        } catch(SQLException ex) {
            System.err.println("An error has occurred while authenticating an user. "
                    + "Error: " + ex);
            
            JsonObject jsonResponse = new JsonObject();
            jsonResponse.addProperty("result", false);
            jsonResponse.addProperty("msg", "An error has occurred. Error: " + ex);
            return jsonResponse.toString();
        } catch (UnsupportedEncodingException ex) {
            System.err.println("An error has occurred while encoding an user's token. "
                    + "Error: " + ex);
            
            JsonObject jsonResponse = new JsonObject();
            jsonResponse.addProperty("result", false);
            jsonResponse.addProperty("msg", "An error has occurred. Error: " + ex);
            return jsonResponse.toString();
        } finally {
            cp.returnConnection(conn);
        }
    }

    @POST
    @Path("logout")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public String logout(@FormParam("token") String token) {
        OConnection conn = cp.getConnection(4);
        if(conn == null){
            JsonObject jsonResponse = new JsonObject();
            jsonResponse.addProperty("result", false);
            jsonResponse.addProperty("msg", "Server is busy.");
            return jsonResponse.toString();
        }
        
        try{
            conn.statement("UPDATE users "
                    + "SET authtoken_use = null "
                    + "WHERE authtoken_use = ?", token);
            conn.commit();
            
            JsonObject jsonResponse = new JsonObject();
            jsonResponse.addProperty("result", true);
            jsonResponse.addProperty("msg", "Disconnection successful");
            return jsonResponse.toString();
        } catch(SQLException ex) {
            System.err.println("An error has occurred while signing out an user "
                    + "(token invalidation). Error: " + ex);
            try {
                System.err.println("Attempting to rollback after error: " + ex);
                conn.rollback();
                System.err.println("Rollback successful for error: " + ex);
            } catch (SQLException ex1) {
                System.err.println("An error has occurred while rolling back "
                        + "after an error Inside the contacts resource. "
                    + "Error: " + ex1);
                System.err.println("Attempting to close connection " + conn.getID());
                try {
                    conn.close();
                } catch (SQLException ex2) {
                    Logger.getLogger(ContactsResource.class.getName()).log(Level.SEVERE, null, ex2);
                }
            }
            
            JsonObject jsonResponse = new JsonObject();
            jsonResponse.addProperty("result", false);
            jsonResponse.addProperty("msg", "An error has occurred. Error: " + ex);
            return jsonResponse.toString();
        } finally {
            cp.returnConnection(conn);
        }
    }
}