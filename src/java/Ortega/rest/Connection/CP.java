package Ortega.rest.Connection;

import Ortega.ConnectionPool.ConnectionPool;

public class CP {
    private int min, max;
    
    public CP(int minConnections, int maxConnections){
        min = minConnections;
        max = maxConnections;
    }
    
    public ConnectionPool getConnectionPool(){
        return new ConnectionPool("localhost", 5432, "IMSP", "postgres", "masterkey", min, max);
    }
}