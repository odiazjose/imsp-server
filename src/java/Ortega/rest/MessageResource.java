package Ortega.rest;

import Ortega.ConnectionPool.ConnectionPool;
import Ortega.Connectivity.OConnection;
import Ortega.rest.Connection.CP;
import com.google.gson.JsonObject;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("Messages")
public class MessageResource {
    private static final ConnectionPool cp = new CP(5, 10).getConnectionPool();
    
    @GET
    @Path("get")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    public String getMessages(
            @QueryParam("mode") @DefaultValue("get") String mode,
            @QueryParam("token") String token,
            @QueryParam("target") String target,
            @QueryParam("offset") @DefaultValue("0") int offset
    ) {
        if(token.isEmpty() || target.isEmpty()){
            JsonObject jsonResponse = new JsonObject();
            jsonResponse.addProperty("result", false);
            jsonResponse.addProperty("msg", "One or more required parameters are empty.");
            return jsonResponse.toString();
        }
        
        OConnection conn = cp.getConnection(5);
        if(conn == null){
            JsonObject jsonResponse = new JsonObject();
            jsonResponse.addProperty("result", false);
            jsonResponse.addProperty("msg", "Server is busy.");
            return jsonResponse.toString();
        }
        
        try{
            Object[][] resultRequester = conn.query("SELECT id_use, email_use FROM users "
                    + "WHERE authtoken_use = ?", token);
            
            if(resultRequester.length > 1){
                int targetID = (int) conn.query("SELECT id_use FROM users "
                    + "WHERE email_use = ?", target)[1][0];
                Object[][] result;
                
                if(mode.equals("update")) result = conn.query("SELECT * FROM " +
                    "(SELECT * FROM message " +
                    "WHERE(sender_msg = ? AND receiver_msg = ? AND read_msg = false) " +
                    "ORDER BY id_msg DESC) AS messages ORDER BY id_msg ASC;", 
                        targetID, resultRequester[1][0]);
                
                else result = conn.query("SELECT * FROM " +
                    "(SELECT * FROM message WHERE((sender_msg = ? AND receiver_msg = ?) " +
                    " OR(sender_msg = ? AND receiver_msg = ?)) ORDER BY id_msg DESC LIMIT 30 OFFSET ?" +
                    ") AS messages ORDER BY id_msg ASC;", 
                        resultRequester[1][0], targetID, targetID, resultRequester[1][0], offset);

                String messages = "[";
                for(int i = 1; i < result.length; i++){
                    String sender, receiver;
                    if((int)result[i][1] == (int)resultRequester[1][0]){
                        sender = (String) resultRequester[1][1];
                        receiver = target;
                    } else {
                        sender = target;
                        receiver = (String) resultRequester[1][1];
                    }
                    
                    String mediaURI = "";
                    byte[] mediaData = (byte[]) result[i][4];
                    if(mediaData.length > 0) mediaURI = new String(mediaData); 
                    
                    JsonObject jsonMsg = new JsonObject();
                    jsonMsg.addProperty("id", (int) result[i][0]);
                    jsonMsg.addProperty("sender", sender);
                    jsonMsg.addProperty("receiver", receiver);
                    jsonMsg.addProperty("content", (String) result[i][3]);
                    jsonMsg.addProperty("media", mediaURI);
                    jsonMsg.addProperty("timestamp", (String) result[i][5]);
                    jsonMsg.addProperty("read", (boolean) result[i][6]);
                    messages += jsonMsg.toString();
                    if(i < result.length - 1) messages += ",";
                }
                messages += "]";

                JsonObject jsonResponse = new JsonObject();
                jsonResponse.addProperty("result", true);
                jsonResponse.addProperty("messages", messages);
                return jsonResponse.toString();
            } else {
                JsonObject jsonResponse = new JsonObject();
                jsonResponse.addProperty("result", false);
                jsonResponse.addProperty("msg", "Your identity could not be validated. "
                        + "Please log in again.");
                return jsonResponse.toString();
            }
        } catch(SQLException ex) {
            System.err.println("An error has occurred while retrieving messages. "
                    + "Error: " + ex);

            JsonObject jsonResponse = new JsonObject();
            jsonResponse.addProperty("result", false);
            jsonResponse.addProperty("msg", "An error has occurred. Error: " + ex);
            return jsonResponse.toString();
        } finally {
            cp.returnConnection(conn);
        }
    }
    
    @GET
    @Path("notify")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    public String notification(@QueryParam("token") String token) {
        if(token.isEmpty()){
            JsonObject jsonResponse = new JsonObject();
            jsonResponse.addProperty("result", false);
            jsonResponse.addProperty("msg", "One or more required parameters are empty.");
            return jsonResponse.toString();
        }
        
        OConnection conn = cp.getConnection(5);
        if(conn == null){
            JsonObject jsonResponse = new JsonObject();
            jsonResponse.addProperty("result", false);
            jsonResponse.addProperty("msg", "Server is busy.");
            return jsonResponse.toString();
        }
        
        try{
            Object[][] resultRequester = conn.query("SELECT id_use FROM users "
                    + "WHERE authtoken_use = ?", token);
            
            if(resultRequester.length > 1){
                Object[][] result = conn.query("SELECT email_use, msgCount "
                    + "FROM users INNER JOIN (SELECT sender_msg, COUNT(sender_msg) "
                        + "AS msgCount FROM message WHERE receiver_msg = ? "
                        + "AND read_msg = false GROUP BY sender_msg) AS senders "
                    + "ON sender_msg = id_use;", 
                        resultRequester[1][0]);

                String messages = "[";
                for(int i = 1; i < result.length; i++){
                    JsonObject jsonMsg = new JsonObject();
                    jsonMsg.addProperty("sender", (String) result[i][0]);
                    jsonMsg.addProperty("msgCount", (long) result[i][1]);
                    messages += jsonMsg.toString();
                    if(i < result.length - 1) messages += ",";
                }
                messages += "]";

                JsonObject jsonResponse = new JsonObject();
                jsonResponse.addProperty("result", true);
                jsonResponse.addProperty("notification", messages);
                return jsonResponse.toString();
            } else {
                JsonObject jsonResponse = new JsonObject();
                jsonResponse.addProperty("result", false);
                jsonResponse.addProperty("msg", "Your identity could not be validated. "
                        + "Please log in again.");
                return jsonResponse.toString();
            }
        } catch(SQLException ex) {
            System.err.println("An error has occurred while retrieving messages. "
                    + "Error: " + ex);

            JsonObject jsonResponse = new JsonObject();
            jsonResponse.addProperty("result", false);
            jsonResponse.addProperty("msg", "An error has occurred. Error: " + ex);
            return jsonResponse.toString();
        } finally {
            cp.returnConnection(conn);
        }
    }

    @POST
    @Path("send")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public String saveMessage(
            @FormParam("token") String token, 
            @FormParam("msg") String msg, 
            @FormParam("media") @DefaultValue("") String media,
            @FormParam("timestamp") String timestamp, 
            @FormParam("target") String target) 
    {
        if((msg.isEmpty() && media.isEmpty()) || timestamp.isEmpty() || target.isEmpty()){
            JsonObject jsonResponse = new JsonObject();
            jsonResponse.addProperty("result", false);
            jsonResponse.addProperty("msg", "One or more required parameters are empty.");
            return jsonResponse.toString();
        }
        
        OConnection conn = cp.getConnection(5);
        if(conn == null){
            JsonObject jsonResponse = new JsonObject();
            jsonResponse.addProperty("result", false);
            jsonResponse.addProperty("msg", "Server is busy.");
            return jsonResponse.toString();
        }
        
        try {
            Object[][] resultSender = conn.query("SELECT id_use FROM users "
                    + "WHERE authtoken_use = ?", token);
            
            if(resultSender.length > 1){
                Object[][] resultTarget = conn.query("INSERT INTO "
                        + "message(sender_msg, receiver_msg, content_msg, "
                        + "media_msg, timestamp_msg) "
                        + "VALUES (?, (SELECT id_use FROM users WHERE email_use = ?), ?, ?, ?)"
                        + "RETURNING id_msg;",
                        resultSender[1][0], target, msg, media.getBytes(), timestamp);
                conn.commit();

                JsonObject jsonResponse = new JsonObject();
                jsonResponse.addProperty("result", true);
                jsonResponse.addProperty("id", (int) resultTarget[1][0]);
                return jsonResponse.toString();
            } else {
                JsonObject jsonResponse = new JsonObject();
                jsonResponse.addProperty("result", false);
                jsonResponse.addProperty("msg", "Your identity could not be validated. "
                        + "Please log in again.");
                return jsonResponse.toString();
            }
        } catch(SQLException ex) {
            System.err.println("An error has occurred while saving a message. "
                    + "Error: " + ex);
            
            try {
                System.err.println("Attempting to rollback after error: " + ex);
                conn.rollback();
                System.err.println("Rollback successful for error: " + ex);
            } catch (SQLException ex1) {
                System.err.println("An error has occurred while rolling back "
                        + "after an error Inside the contacts resource. "
                    + "Error: " + ex1);
                System.err.println("Attempting to close connection " + conn.getID());
                try {
                    conn.close();
                } catch (SQLException ex2) {
                    Logger.getLogger(ContactsResource.class.getName()).log(Level.SEVERE, null, ex2);
                }
            }
            
            JsonObject jsonResponse = new JsonObject();
            jsonResponse.addProperty("result", false);
            jsonResponse.addProperty("msg", "An error has occurred. Error: " + ex);
            return jsonResponse.toString();
        } finally {
            cp.returnConnection(conn);
        }
    }
    
    @POST
    @Path("read")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public String markRead(@FormParam("messages") String messages) {
        if(messages.isEmpty()) return "{\"result\": false}";
        String[] msgID = messages.split(",");
        
        OConnection conn = cp.getConnection(5);
        if(conn == null){
            JsonObject jsonResponse = new JsonObject();
            jsonResponse.addProperty("result", false);
            jsonResponse.addProperty("msg", "Server is busy.");
            return jsonResponse.toString();
        }
        
        String stm = "UPDATE message SET read_msg = true WHERE id_msg IN(";
        Object[] values = new Object[msgID.length];
        
        for(int i = 0; i < msgID.length; i++){
            stm += "?";
            if(i < msgID.length - 1) stm += ",";
            else stm += ");";
            
            values[i] = Integer.parseInt(msgID[i]);
        }
        
        try {
            conn.statement(stm, values);
            conn.commit();
            
            JsonObject jsonResponse = new JsonObject();
            jsonResponse.addProperty("result", true);
            return jsonResponse.toString();
        } catch(SQLException ex) {
            System.err.println("An error has occurred while updating a message. "
                    + "Error: " + ex);
            
            try {
                System.err.println("Attempting to rollback after error: " + ex);
                conn.rollback();
                System.err.println("Rollback successful for error: " + ex);
            } catch (SQLException ex1) {
                System.err.println("An error has occurred while rolling back "
                        + "after an error Inside the contacts resource. "
                    + "Error: " + ex1);
                System.err.println("Attempting to close connection " + conn.getID());
                try {
                    conn.close();
                } catch (SQLException ex2) {
                    Logger.getLogger(ContactsResource.class.getName()).log(Level.SEVERE, null, ex2);
                }
            }
            
            JsonObject jsonResponse = new JsonObject();
            jsonResponse.addProperty("result", false);
            jsonResponse.addProperty("msg", "Internal server error.");
            return jsonResponse.toString();
        } finally {
            cp.returnConnection(conn);
        }
    }
}