package Ortega.rest;

import Ortega.ConnectionPool.ConnectionPool;
import Ortega.Connectivity.OConnection;
import Ortega.rest.Connection.CP;
import com.google.gson.JsonObject;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("Contacts")
public class ContactsResource {
    private static final ConnectionPool cp = new CP(2, 3).getConnectionPool();
    
    @GET
    @Path("get")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    public String getContacts(@QueryParam("token") String token) {
        OConnection conn = cp.getConnection(3);
        if(conn == null){
            JsonObject jsonResponse = new JsonObject();
            jsonResponse.addProperty("result", false);
            jsonResponse.addProperty("msg", "Server is busy.");
            return jsonResponse.toString();
        }
        
        try{
            Object[][] resultRequester = conn.query("SELECT id_use FROM users "
                    + "WHERE authtoken_use = ?", token);
            
            if(resultRequester.length > 1){
                Object[][] result = conn.query("SELECT "
                    + "CAST(json_agg(row_to_json(contact)) AS TEXT) AS contacts "
                    + "FROM (SELECT email_use AS email, name_use AS name FROM users "
                        + "INNER JOIN contact ON id_con = users.id_use  "
                    + "WHERE contact.id_use = ?) AS contact;", 
                        resultRequester[1][0]);

                JsonObject jsonResponse = new JsonObject();
                jsonResponse.addProperty("result", true);
                jsonResponse.addProperty("contacts", (String) result[1][0]);
                return jsonResponse.toString();
            } else {
                JsonObject jsonResponse = new JsonObject();
                jsonResponse.addProperty("result", false);
                jsonResponse.addProperty("msg", "Your identity could not be validated. "
                        + "Please log in again.");
                return jsonResponse.toString();
            }
        } catch(SQLException ex) {
            System.err.println("An error has occurred while getting contacts. "
                    + "Error: " + ex);
            
            JsonObject jsonResponse = new JsonObject();
            jsonResponse.addProperty("result", false);
            jsonResponse.addProperty("msg", "An error has occurred. Error: " + ex);
            return jsonResponse.toString();
        } finally {
            cp.returnConnection(conn);
        }
    }

    @POST
    @Path("add")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public String addContact(
            @FormParam("token") String token, 
            @FormParam("target") String target) 
    {
        if(target.isEmpty()){
            JsonObject jsonResponse = new JsonObject();
            jsonResponse.addProperty("result", false);
            jsonResponse.addProperty("msg", "Empty contact address.");
            return jsonResponse.toString();
        }
        
        OConnection conn = cp.getConnection(3);
        if(conn == null){
            JsonObject jsonResponse = new JsonObject();
            jsonResponse.addProperty("result", false);
            jsonResponse.addProperty("msg", "Server is busy.");
            return jsonResponse.toString();
        }
        
        try{
            Object[][] resultRequester = conn.query("SELECT id_use FROM users "
                    + "WHERE authtoken_use = ?", token);
            
            if(resultRequester.length > 1){
                conn.statement("INSERT INTO contact(id_use, id_con) "
                        + "VALUES(?, (SELECT id_use FROM users WHERE email_use = ?))",
                        resultRequester[1][0], target);
                conn.commit();

                JsonObject jsonResponse = new JsonObject();
                jsonResponse.addProperty("result", true);
                jsonResponse.addProperty("msg", "Contact has been added.");
                return jsonResponse.toString();
            } else {
                JsonObject jsonResponse = new JsonObject();
                jsonResponse.addProperty("result", false);
                jsonResponse.addProperty("msg", "Your identity could not be validated. "
                        + "Please log in again.");
                return jsonResponse.toString();
            }
        } catch(SQLException ex) {
            try {
                System.err.println("Attempting to rollback after error: " + ex);
                conn.rollback();
                System.err.println("Rollback successful for error: " + ex);
            } catch (SQLException ex1) {
                System.err.println("An error has occurred while rolling back "
                        + "after an error Inside the contacts resource. "
                    + "Error: " + ex1);
                System.err.println("Attempting to close connection " + conn.getID());
                try {
                    conn.close();
                } catch (SQLException ex2) {
                    Logger.getLogger(ContactsResource.class.getName()).log(Level.SEVERE, null, ex2);
                }
            }
            
            System.err.println("An error has occurred while adding a contact. "
                    + "Error: " + ex);
            
            JsonObject jsonResponse = new JsonObject();
            jsonResponse.addProperty("result", false);
            if(ex.getSQLState().equals("23503")) 
                jsonResponse.addProperty("msg", "Email address: " + target + " does not exist.");
            else jsonResponse.addProperty("msg", "An error has occurred. Error: " + ex);
            return jsonResponse.toString();
        } finally {
            cp.returnConnection(conn);
        }
    }
}